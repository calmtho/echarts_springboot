package com.tho;

import com.tho.entity.MtDeptPersonDTO;
import com.tho.entity.MtPerson;
import com.tho.mapper.MtDeptPersonDTOMapper;
import com.tho.mapper.MtPersonMapper;
import com.tho.service.MtPersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class BootEchartsApplicationTests {

    @Autowired
    private MtPersonMapper mtPersonMapper;

    @Autowired
    private MtDeptPersonDTOMapper deptPersonDTOMapper;


    @Test
    void contextLoads() {
        List<MtPerson> all = mtPersonMapper.findAll();
        all.stream().forEach(System.out::println);
    }

    @Test
    public  void testCount(){
        List<MtDeptPersonDTO> deptPersonDTOList= deptPersonDTOMapper.findOneById(113l);
        deptPersonDTOList.stream().forEach(System.out::println);
    }

    @Test
    public  void testForeachList(){
        List<Long> list=new ArrayList<>();
        list.add(0,113L);
        list.add(1,117L);
        List<MtDeptPersonDTO> dtoList = deptPersonDTOMapper.findDTOList(list);
        dtoList.stream().forEach(System.out::println);
    }


    @Autowired
    private MtPersonService mtPersonService;
    @Test
    public  void testService(){
        List<MtDeptPersonDTO> deptPersonDTOList = mtPersonService.deptPeronStatistics();
        deptPersonDTOList.stream().forEach(System.out::println);
    }

    //节点Id
    @Value("${mt.nodeDeptId}")
    String nodeId;

    @Test
    public void testNodeDeptId(){
        List<Long> deptIdNodes = mtPersonMapper.findDeptIdNodes(nodeId);
        List<MtDeptPersonDTO> dtoList = deptPersonDTOMapper.findDTOList(deptIdNodes);
        dtoList.stream().forEach(System.out::println);
    }

    @Test
    public void testSerNodeDeptId(){
        List<MtDeptPersonDTO> deptPersonDTOList = mtPersonService.deptPeronStatistics();
        deptPersonDTOList.stream().forEach(System.out::println);
    }
}

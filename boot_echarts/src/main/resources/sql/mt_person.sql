/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : ry

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 18/01/2021 17:34:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mt_person
-- ----------------------------
DROP TABLE IF EXISTS `mt_person`;
CREATE TABLE `mt_person`  (
  `person_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '项目人员ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '项目人员所属部门ID',
  `realname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目人员姓名',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `QQ` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'QQ账号',
  `VPN_username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'VPN账号',
  `VPN_password` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'VPN密码',
  `zentao_username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '禅道账户',
  `zentao_password` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '禅道密码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户状态（0在职 1离职）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`person_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '项目人员以及账号信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mt_person
-- ----------------------------
INSERT INTO `mt_person` VALUES (1, 113, '测试', '213231', '2132131', '312', '31231', '1312', '321313', '1231', '1', '', '2020-11-03 15:32:14', '', '2020-11-19 14:09:55');
INSERT INTO `mt_person` VALUES (2, 110, '测试', '', '', '', '', '', '', '', '0', '', '2020-11-06 15:40:04', '', NULL);
INSERT INTO `mt_person` VALUES (3, 114, '哈哈哈哈', '', '', '', '', '', '', '', '0', '', '2020-11-25 09:43:27', '', NULL);
INSERT INTO `mt_person` VALUES (4, 114, '小心', '', '', '', '', '', '', '', '0', '', '2020-11-25 09:43:41', '', NULL);
INSERT INTO `mt_person` VALUES (5, 113, 'bug来了', '', '', '', '', '', '', '', '0', '', '2020-11-25 09:43:54', '', NULL);
INSERT INTO `mt_person` VALUES (6, 117, '数据清洗', '', '', '', '', '', '', '', '0', '', '2020-11-25 09:44:18', '', NULL);
INSERT INTO `mt_person` VALUES (7, 113, 'dsgfdgfjhgkjll', '', '', '', '', '', '', '', '0', '', '2021-01-13 10:58:10', '', NULL);
INSERT INTO `mt_person` VALUES (8, 113, 'nmbvcxcbnmhkjghjhk', '', '', '', '', '', '', '', '0', '', '2021-01-13 10:58:25', '', NULL);
INSERT INTO `mt_person` VALUES (9, 113, 'mnbmnvnvcb', '', '', '', '', '', '', '', '0', '', '2021-01-13 10:58:42', '', NULL);
INSERT INTO `mt_person` VALUES (10, NULL, 'mnbvcxzxcvb', '', '', '', '', '', '', '', '0', '', '2021-01-13 10:59:34', '', NULL);
INSERT INTO `mt_person` VALUES (11, 117, '2', '', '', '', '', '', '', '', '0', '', '2021-01-14 16:17:27', '', NULL);

SET FOREIGN_KEY_CHECKS = 1;

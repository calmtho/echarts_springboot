/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : ry

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 18/01/2021 17:33:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '铭太科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-10-23 11:42:15', 'admin', '2020-10-26 15:00:12');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '其他部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-10-23 11:42:15', 'admin', '2020-10-26 15:00:12');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-10-23 11:42:15', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-10-23 11:42:15', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-10-23 11:42:15', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-10-23 11:42:15', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-10-23 11:42:15', '', NULL);
INSERT INTO `sys_dept` VALUES (110, 100, '0,100', '事业一部', 1, '熊杰', '13040402321', '21321@163.com', '0', '0', 'admin', '2020-10-26 10:20:08', 'admin', '2020-10-26 14:56:46');
INSERT INTO `sys_dept` VALUES (111, 110, '0,100,110', '总部', 1, '唐娟', '', '', '0', '0', 'admin', '2020-10-26 10:21:13', 'admin', '2020-10-26 14:39:00');
INSERT INTO `sys_dept` VALUES (112, 110, '0,100,110', '各地实施', 2, '', '', '', '0', '0', 'admin', '2020-10-26 10:24:50', 'admin', '2020-10-26 14:56:46');
INSERT INTO `sys_dept` VALUES (113, 111, '0,100,110,111', '项目管理部门', 1, '江狄霏', '', '', '0', '0', 'admin', '2020-10-26 14:31:11', 'admin', '2020-10-26 14:38:21');
INSERT INTO `sys_dept` VALUES (114, 111, '0,100,110,111', '测试部署国库', 2, '李裕林', NULL, NULL, '0', '0', 'admin', '2020-10-26 14:32:18', '', NULL);
INSERT INTO `sys_dept` VALUES (115, 111, '0,100,110,111', '数据分析部门', 3, '唐娟', '', '', '0', '0', 'admin', '2020-10-26 14:33:56', 'admin', '2020-10-26 14:39:00');
INSERT INTO `sys_dept` VALUES (116, 111, '0,100,110,111', '技术开发部门', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:35:28', '', NULL);
INSERT INTO `sys_dept` VALUES (117, 115, '0,100,110,111,115', '数据清洗', 1, '项洋', NULL, NULL, '0', '0', 'admin', '2020-10-26 14:40:09', '', NULL);
INSERT INTO `sys_dept` VALUES (118, 115, '0,100,110,111,115', '数据分析', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:40:24', '', NULL);
INSERT INTO `sys_dept` VALUES (119, 116, '0,100,110,111,116', '老系统', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:40:43', '', NULL);
INSERT INTO `sys_dept` VALUES (120, 116, '0,100,110,111,116', '新系统', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:40:54', '', NULL);
INSERT INTO `sys_dept` VALUES (121, 112, '0,100,110,112', '华南区', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:42:45', 'admin', '2020-10-26 14:51:28');
INSERT INTO `sys_dept` VALUES (122, 112, '0,100,110,112', '华东区', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:43:02', '', NULL);
INSERT INTO `sys_dept` VALUES (123, 112, '0,100,110,112', '西北区', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:43:26', 'admin', '2020-10-26 14:56:46');
INSERT INTO `sys_dept` VALUES (124, 112, '0,100,110,112', '驻点运维', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:43:59', '', NULL);
INSERT INTO `sys_dept` VALUES (125, 121, '0,100,110,112,121', '广州市本级', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:50:28', '', NULL);
INSERT INTO `sys_dept` VALUES (126, 121, '0,100,110,112,121', '广州区镇', 2, '', '', '', '0', '0', 'admin', '2020-10-26 14:51:05', 'admin', '2020-10-26 14:51:28');
INSERT INTO `sys_dept` VALUES (127, 121, '0,100,110,112,121', '湖南区域', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:51:49', '', NULL);
INSERT INTO `sys_dept` VALUES (128, 122, '0,100,110,112,122', '江西区域', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:52:13', '', NULL);
INSERT INTO `sys_dept` VALUES (129, 122, '0,100,110,112,122', '浙江区域', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:52:32', '', NULL);
INSERT INTO `sys_dept` VALUES (130, 123, '0,100,110,112,123', '陕西地区', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:53:40', '', NULL);
INSERT INTO `sys_dept` VALUES (131, 123, '0,100,110,112,123', '甘肃地区', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:54:17', '', NULL);
INSERT INTO `sys_dept` VALUES (132, 123, '0,100,110,112,123', '青海地区', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:54:55', '', NULL);
INSERT INTO `sys_dept` VALUES (133, 123, '0,100,110,112,123', '四川地区', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-10-26 14:55:46', '', NULL);
INSERT INTO `sys_dept` VALUES (134, 123, '0,100,110,112,123', '河南地区', 5, '', '', '', '0', '0', 'admin', '2020-10-26 14:56:22', 'admin', '2020-10-26 14:56:46');
INSERT INTO `sys_dept` VALUES (135, 111, '0,100,110,111', '管理层', 1, '熊杰', NULL, NULL, '0', '0', 'admin', '2021-01-14 14:33:14', '', NULL);

SET FOREIGN_KEY_CHECKS = 1;

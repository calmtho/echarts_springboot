package com.tho;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "com.tho.mapper")
public class BootEchartsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootEchartsApplication.class, args);
    }

}

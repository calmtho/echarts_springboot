package com.tho.controller;

import com.tho.entity.MtDeptPersonDTO;
import com.tho.service.MtPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MtPersonController {
    @Autowired
    private MtPersonService mtPersonService;

    @RequestMapping("/person/getdata")
    @ResponseBody
    public List<MtDeptPersonDTO> showData(){
        List<MtDeptPersonDTO> statistics = mtPersonService.deptPeronStatistics();
        return statistics;
    }

    @RequestMapping("/personStatistics")
    public String personStatistics(){
        return "/person/personStatistics";
    }
    @RequestMapping("/")
    public String index(){
        return "index";
    }
}

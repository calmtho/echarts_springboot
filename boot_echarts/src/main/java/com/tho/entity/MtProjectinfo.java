package com.tho.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class MtProjectinfo {
    /** 项目ID */
    private Long projectId;

    /** 区域ID */
    private Long areaId;

    /** 项目名称 */
    private String projectName;

    /** 项目类型（0未知 1实施，2运维 3APP 4硬件 5平板适配） */
    private String projetType;

    /** 中标日期 */
    private Date hitDate;

    /** 签约日期 */
    private Date contractDate;

    /** 签约金额 */
    private BigDecimal contractSum;

    /** 已收金额 */
    private BigDecimal received;

    /** 未收金额 */
    private BigDecimal uncollected;

    /** 收款比例 */
    private BigDecimal ratio;

    /** 立项日期 */
    private Date establishedDate;

    /** 合同上线日期 */
    private Date expiry;

    /** 进场日期 */
    private Date entry;

    /** 初验收日期 */
    private Date precherk;

    /** 最终验收日期 */
    private Date finalCherk;

    /** 项目进度状态(0未知 1筹备中 2实施中 3已初验 4已终验收 5返工中 6运维中 7中止合同） */
    private String rate;

    /** 运维类型(0未知，1不运维，2收费运维，3免费运维) */
    private String operation;

    /** 验收类型(0未知 1未验收 2已初验 3已终验） */
    private String cherk;

    /** 预计验收日期 */
    private Date expectedCherk;

    /** 预计验收复杂度(0未知 1高 2中 3低） */
    private String cherkLevel;

    /** 项目负责人 */
    private String leader;

    /** 项目级别(0未知 1高 2中 3低） */
    private String projectLevel;

    private String independent;

    /** 状态（0正常 1停用） */
    private String status;

    /**关联项目区域*/
   // private MtArea mtArea;
}

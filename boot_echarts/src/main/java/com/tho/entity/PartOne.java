package com.tho.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Component;


@Data
@ToString
@Component
public class PartOne{

//     @Value("${mt.partOneId}")
//     String partOneId;

     private MtPerson mtPerson;
     //private MtProjectinfo mtProjectinfo;
     private SysDept sysDept;


}

package com.tho.entity;

import lombok.Data;
import org.springframework.stereotype.Component;


@Data
@Component
public class MtDeptPersonDTO {

     private Long deptId;

     private String deptName;

     //private String realName;
     private String nums;
}

package com.tho.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class MtPerson {
    /** 项目人员ID */
    private Long personId;

    /** 项目人员所属部门ID */
    private Long deptId;

    /** 项目人员姓名 */
    private String realname;

    /** 联系电话 */
    private String phone;

    /** QQ账号 */
    private String QQ;

    /** VPN账号 */
    private String vpnUsername;

    /** VPN密码 */
    private String vpnPassword;

    /** 禅道账户 */
    private String zentaoUsername;

    /** 禅道密码 */
    private String zentaoPassword;

    /** 邮箱 */
    private String email;

    /** 用户状态（0在职 1离职） */
    private String status;

    private SysDept sysDept;
}

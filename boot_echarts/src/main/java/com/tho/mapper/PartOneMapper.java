package com.tho.mapper;

import com.tho.entity.PartOne;

public interface PartOneMapper {

    PartOne findOneById(Long deptId);
}

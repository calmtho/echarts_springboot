package com.tho.mapper;

import com.tho.entity.MtDeptPersonDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MtDeptPersonDTOMapper {

    List<MtDeptPersonDTO> findOneById(Long deptId);

    List<MtDeptPersonDTO> findDTOList(List<Long> deptId);
}

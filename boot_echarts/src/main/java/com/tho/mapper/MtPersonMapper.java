package com.tho.mapper;

import com.tho.entity.MtPerson;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface MtPersonMapper {




//    @Select("select * from mt_person")
//    @Results({
//            @Result( id = true ,property = "personId", column = "person_id"),
//            @Result(property="deptId" ,column="dept_id")
//    })
    List<MtPerson> findAll();

//    @Select("SELECT d.dept_id FROM  mt_person p " +
//            "left JOIN sys_dept d ON d.dept_id=p.dept_id WHERE p.dept_id  IS NOT NULL " +
//            "AND d.ancestors LIKE '%110%' GROUP BY d.dept_name" )
    //注释了上面写死的方式，使用@value的方式动态获取值
    List<Long> findDeptIdNodes(String ancestors);




}

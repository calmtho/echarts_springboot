package com.tho.service.impl;

import com.tho.entity.MtDeptPersonDTO;
import com.tho.mapper.MtDeptPersonDTOMapper;
import com.tho.mapper.MtPersonMapper;
import com.tho.service.MtPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MtPersonServiceimpl implements MtPersonService {
    @Value("${mt.nodeDeptId}")
    String nodeDeptId;

    @Autowired
    private MtDeptPersonDTOMapper deptPersonDTOMapper;

    @Autowired
    private MtPersonMapper personMapper;

    @Override
    public List<MtDeptPersonDTO> deptPeronStatistics() {
        //通过事业一部节点id找到该部门下的最小节点合集，规定人员不能挂在父节点，只能是最小子节点
        List<Long> deptIdNodes = personMapper.findDeptIdNodes(nodeDeptId);
        //通过合集统计挂载了节点的人
        List<MtDeptPersonDTO> countList = deptPersonDTOMapper.findDTOList(deptIdNodes);
        return countList;
    }
}

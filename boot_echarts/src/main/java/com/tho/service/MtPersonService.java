package com.tho.service;

import com.tho.entity.MtDeptPersonDTO;

import java.util.List;

public interface MtPersonService {
    public List<MtDeptPersonDTO> deptPeronStatistics();
}
